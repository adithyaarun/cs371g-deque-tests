// -------------
// TestDeque.cpp
// -------------

// http://www.cplusplus.com/reference/deque/deque/

// --------
// includes
// --------

#include <deque>     // deque
#include <stdexcept> // invalid_argument, out_of_range

#include "gtest/gtest.h"

#include "Deque.hpp"

// ------
// usings
// ------

using namespace std;
using namespace testing;

// -----
// Types
// -----

template <typename T>
struct DequeFixture : Test {
    using deque_type    = T;
    using allocator_type = typename deque_type::allocator_type;
    using iterator       = typename deque_type::iterator;
    using const_iterator = typename deque_type::const_iterator;};

using deque_types =
    Types<
        my_deque<int>,
           deque<int>,
        my_deque<int, allocator<int>>,
           deque<int, allocator<int>>,
        my_deque<deque<int>>,
           deque<deque<int>>,
        my_deque<deque<int>, allocator<deque<int>>>,
           deque<deque<int>, allocator<deque<int>>>
           >;

#ifdef __APPLE__
    TYPED_TEST_CASE(DequeFixture, deque_types,);
#else
    TYPED_TEST_CASE(DequeFixture, deque_types);
#endif

// -----
// Tests
// -----

TYPED_TEST(DequeFixture, test0) {
    using deque_type = typename TestFixture::deque_type;
    const deque_type x;
    ASSERT_TRUE(x.empty());
    EXPECT_TRUE(x.size() == 0u);}

TYPED_TEST(DequeFixture, test1) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x;
    iterator b = begin(x);
    iterator e = end(x);
    EXPECT_TRUE(b == e);}

TYPED_TEST(DequeFixture, test2) {
    using deque_type     = typename TestFixture::deque_type;
    using const_iterator = typename TestFixture::const_iterator;
    const deque_type x;
    const_iterator b = begin(x);
    const_iterator e = end(x);
    EXPECT_TRUE(b == e);}


// ------------
// constructors
// ------------

//default
TYPED_TEST(DequeFixture, cns0) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    EXPECT_TRUE(x.empty());
    EXPECT_TRUE(x.begin() == x.end());
    EXPECT_TRUE(x.size() == 0u);}

//size
TYPED_TEST(DequeFixture, cns1_0) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x(73);
    EXPECT_TRUE(!x.empty());
    EXPECT_TRUE(x.begin() != x.end());
    EXPECT_TRUE(x.size() == 73u);}

//size
TYPED_TEST(DequeFixture, cns1_1) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x(0);
    EXPECT_TRUE(x.empty());
    EXPECT_TRUE(x.begin() == x.end());
    EXPECT_TRUE(x.size() == 0u);}

//fill
TYPED_TEST(DequeFixture, cns2) {
    using deque_type = typename TestFixture::deque_type;
    typename deque_type::value_type a(5);
    deque_type x(73, a);
    EXPECT_TRUE(!x.empty());
    EXPECT_TRUE(x.begin() != x.end());
    EXPECT_TRUE(x.size() == 73u);
    EXPECT_TRUE(x.at(12) == a);}

//fill + allocator
TYPED_TEST(DequeFixture, cns3) {
    using deque_type = typename TestFixture::deque_type;
    typename TestFixture::allocator_type alloc;
    typename deque_type::value_type a(5);
    deque_type x(73, a, alloc);
    EXPECT_TRUE(!x.empty());
    EXPECT_TRUE(x.begin() != x.end());
    EXPECT_TRUE(x.size() == 73u);
    EXPECT_TRUE(x.at(12) == a);}

//initializer list
TYPED_TEST(DequeFixture, cns4) {
    using deque_type = typename TestFixture::deque_type;
    typename deque_type::value_type a(1);
    typename deque_type::value_type b(2);
    typename deque_type::value_type c(3);
    deque_type x = {a, b, c};
    EXPECT_TRUE(!x.empty());
    EXPECT_TRUE(x.begin() != x.end());
    EXPECT_TRUE(x.size() == 3u);
    EXPECT_TRUE(x.at(2) == c);}

//initializer list + alloc
TYPED_TEST(DequeFixture, cns5) {
    using deque_type = typename TestFixture::deque_type;
    typename TestFixture::allocator_type alloc;
    typename deque_type::value_type a(1);
    typename deque_type::value_type b(2);
    typename deque_type::value_type c(3);
    deque_type x({a, b, c}, alloc);
    EXPECT_TRUE(!x.empty());
    EXPECT_TRUE(x.begin() != x.end());
    EXPECT_TRUE(x.size() == 3u);
    EXPECT_TRUE(x.at(2) == c);}

//copy constructor
TYPED_TEST(DequeFixture, cns6) {
    using deque_type = typename TestFixture::deque_type;
    typename deque_type::value_type a(1);
    typename deque_type::value_type b(2);
    typename deque_type::value_type c(3);
    deque_type x({a, b, c});
    deque_type y(x);
    EXPECT_TRUE(!y.empty());
    EXPECT_TRUE(y.begin() != y.end());
    EXPECT_TRUE(y.size() == 3u);
    EXPECT_TRUE(y.at(2) == c);}

// --------------
// iterator tests
// --------------

TYPED_TEST(DequeFixture, it0) {
    using deque_type = typename TestFixture::deque_type;
    using iterator = typename TestFixture::iterator;
    typename deque_type::value_type a(1);
    typename deque_type::value_type b(2);
    typename deque_type::value_type c(3);
    deque_type x({a, b, c});
    iterator it = x.begin();
    EXPECT_TRUE(*it == a);
    ++it;
    EXPECT_TRUE(*it == b);
    ++it;
    EXPECT_TRUE(*it == c);
    ++it;
    EXPECT_TRUE(it == x.end());}

TYPED_TEST(DequeFixture, it1) {
    using deque_type = typename TestFixture::deque_type;
    using iterator = typename TestFixture::iterator;
    typename deque_type::value_type a(1);
    typename deque_type::value_type b(2);
    typename deque_type::value_type c(3);
    deque_type x({a, b, c});
    iterator it = x.end();
    --it;
    EXPECT_TRUE(*it == c);
    --it;
    EXPECT_TRUE(*it == b);
    --it;
    EXPECT_TRUE(*it == a);
    EXPECT_TRUE(it == x.begin());}

TYPED_TEST(DequeFixture, it2) {
    using deque_type = typename TestFixture::deque_type;
    using iterator = typename TestFixture::iterator;
    typename deque_type::value_type a(1);
    typename deque_type::value_type b(2);
    typename deque_type::value_type c(3);
    deque_type x({a, b, c});
    iterator it = x.begin();
    EXPECT_TRUE(*(it + 2) == c);}

TYPED_TEST(DequeFixture, it3) {
    using deque_type = typename TestFixture::deque_type;
    using iterator = typename TestFixture::iterator;
    typename deque_type::value_type a(1);
    typename deque_type::value_type b(2);
    typename deque_type::value_type c(3);
    deque_type x({a, b, c});
    iterator it = x.end();
    EXPECT_TRUE(*(it - 3) == a);}

TYPED_TEST(DequeFixture, it4) {
    using deque_type = typename TestFixture::deque_type;
    using iterator = typename TestFixture::iterator;
    typename deque_type::value_type a(1);
    deque_type x(50, a);
    iterator it = x.begin();
    while(it != x.end()) {
        EXPECT_TRUE(*it == a);
        ++it;
    }}

// -------------------------
// non-growth modifier tests
// -------------------------

TYPED_TEST(DequeFixture, ng0) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    typename deque_type::value_type a;
    x.push_back(a);
    x.push_back(a);
    x.push_back(a);
    EXPECT_TRUE(x.size() == 3u);}

TYPED_TEST(DequeFixture, ng1) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    typename deque_type::value_type a(0);
    typename deque_type::value_type b(1);
    x.push_back(a);
    x.push_back(a);
    x.push_back(b);
    x.pop_front();
    x.pop_front();
    EXPECT_TRUE(x.size() == 1u);
    EXPECT_TRUE(x.at(0) == b);}

TYPED_TEST(DequeFixture, ng2) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    typename deque_type::value_type a;
    x.push_back(a);
    x.push_back(a);
    x.push_back(a);
    x.pop_front();
    x.pop_front();
    x.pop_front();
    EXPECT_TRUE(x.size() == 0u);}

TYPED_TEST(DequeFixture, ng3) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    typename deque_type::value_type a;
    x.push_back(a);
    x.push_back(a);
    x.push_back(a);
    x.push_back(a);
    x.pop_front();
    x.pop_front();
    x.pop_front();
    x.push_front(a);
    x.pop_back();
    EXPECT_TRUE(x.size() == 1u);}

TYPED_TEST(DequeFixture, ng4) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    typename deque_type::value_type a;
    x.push_back(a);
    x.push_back(a);
    typename deque_type::iterator i = x.begin();
    ++i;
    typename deque_type::value_type b(1);
    typename deque_type::value_type c(2);
    typename deque_type::value_type d(3);
    x.insert(i, b);
    x.insert(i, c);
    x.insert(i, d);
    EXPECT_TRUE(x.size() == 5u);
    EXPECT_TRUE(x.at(1) == d);
    EXPECT_TRUE(x.at(3) == b);}

TYPED_TEST(DequeFixture, ng5) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    typename deque_type::value_type a;
    x.push_back(a);
    x.push_back(a);
    typename deque_type::iterator i = x.end();
    --i;
    x.erase(i);
    EXPECT_TRUE(x.size() == 1u);}

TYPED_TEST(DequeFixture, ng6) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    typename deque_type::value_type a(0);
    typename deque_type::value_type b(1);
    x.push_back(a);
    x.push_back(a);
    x.push_back(b);
    x.push_back(a);
    x.push_back(a);
    deque_type y;
    x.swap(y);
    EXPECT_TRUE(y.size() == 5u);
    EXPECT_TRUE(x.size() == 0u);}

TYPED_TEST(DequeFixture, ng7) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    typename deque_type::value_type a;
    x.push_back(a);
    x.push_back(a);
    x.push_back(a);
    x.push_back(a);
    x.push_back(a);
    x.clear();
    EXPECT_TRUE(x.size() == 0u);}

// --------------------------
// modifier tests with growth
// --------------------------

TYPED_TEST(DequeFixture, gro0) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    for(int i = 0; i < 50; ++i) {
        typename deque_type::value_type a(i);
        x.push_back(a);
    }
    typename deque_type::value_type a(40);
    EXPECT_TRUE(x.size() == 50u);
    EXPECT_TRUE(x.at(40) == a);}

TYPED_TEST(DequeFixture, gro0_1) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    for(int i = 0; i < 50; ++i) {
        typename deque_type::value_type a(i);
        x.push_front(a);
    }
    typename deque_type::value_type a(9);
    EXPECT_TRUE(x.size() == 50u);
    EXPECT_TRUE(x.at(40) == a);}


TYPED_TEST(DequeFixture, gro1) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    for(int i = 0; i < 50; ++i) {
        typename deque_type::value_type a(i);
        x.push_back(a);
    }
    for(int i = 0; i < 38; ++i) {
        x.pop_back();
    }
    typename deque_type::value_type a(8);
    EXPECT_TRUE(x.size() == 12u);
    EXPECT_TRUE(x.at(8) == a);}


TYPED_TEST(DequeFixture, gro1_1) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    for(int i = 0; i < 50; ++i) {
        typename deque_type::value_type a(i);
        x.push_front(a);
    }
    for(int i = 0; i < 38; ++i) {
        x.pop_front();
    }
    typename deque_type::value_type a(3);
    EXPECT_TRUE(x.size() == 12u);
    EXPECT_TRUE(x.at(8) == a);}

TYPED_TEST(DequeFixture, gro2) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    for(int i = 0; i < 50; ++i) {
        typename deque_type::value_type a(i);
        x.push_back(a);
    }
    for(int i = 0; i < 38; ++i) {
        x.pop_front();
    }
    typename deque_type::value_type a(42);
    EXPECT_TRUE(x.size() == 12u);
    EXPECT_TRUE(x.at(4) == a);}

TYPED_TEST(DequeFixture, gro2_1) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    for(int i = 0; i < 50; ++i) {
        typename deque_type::value_type a(i);
        x.push_front(a);
    }
    for(int i = 0; i < 38; ++i) {
        x.pop_back();
    }
    typename deque_type::value_type a(45);
    EXPECT_TRUE(x.size() == 12u);
    EXPECT_TRUE(x.at(4) == a);}

TYPED_TEST(DequeFixture, gro3) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    for(int i = 0; i < 50; ++i) {
        typename deque_type::value_type a;
        x.push_back(a);
    }
    for(int i = 0; i < 38; ++i) {
        x.pop_front();
    }
    typename deque_type::value_type a(35);
    x.push_front(a);
    for(int i = 0; i < 12; ++i) {
        x.pop_back();
    }
    EXPECT_TRUE(x.size() == 1u);
    EXPECT_TRUE(x.at(0) == a);}

TYPED_TEST(DequeFixture, gro3_1) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    for(int i = 0; i < 50; ++i) {
        typename deque_type::value_type a;
        x.push_front(a);
    }
    for(int i = 0; i < 38; ++i) {
        x.pop_back();
    }
    typename deque_type::value_type a(35);
    x.push_back(a);
    for(int i = 0; i < 12; ++i) {
        x.pop_front();
    }
    EXPECT_TRUE(x.size() == 1u);
    EXPECT_TRUE(x.at(0) == a);}

TYPED_TEST(DequeFixture, gro4) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    typename deque_type::value_type a(0);
    x.push_back(a);
    x.push_back(a);
    for(int i = 0; i < 50; ++i) {
        typename deque_type::iterator it = x.begin();
        ++it;
        typename deque_type::value_type b(i);
        x.insert(it, b);
    }
    typename deque_type::value_type c(1);
    typename deque_type::value_type d(48);
    EXPECT_TRUE(x.size() == 52u);
    EXPECT_TRUE(x.at(0) == a);
    EXPECT_TRUE(x.at(2) == d);
    EXPECT_TRUE(x.at(49) == c);}

TYPED_TEST(DequeFixture, gro4_1) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    typename deque_type::value_type a(0);
    x.push_front(a);
    x.push_front(a);
    for(int i = 0; i < 50; ++i) {
        typename deque_type::iterator it = x.begin();
        ++it;
        typename deque_type::value_type b(i);
        x.insert(it, b);
    }
    for(int i = 0; i < 20; ++i) {
        typename deque_type::iterator it = x.begin();
        ++it;
        x.erase(it);
    }
    EXPECT_TRUE(x.size() == 32u);}

TYPED_TEST(DequeFixture, gro5) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    typename deque_type::value_type a(0);
    x.push_front(a);
    for(int i = 0; i < 50; ++i) {
        typename deque_type::iterator it = x.begin();
        ++it;
        typename deque_type::value_type b(i);
        x.insert(it, b);
    }
    deque_type y = x;
    deque_type z(y);
    EXPECT_TRUE(x.size() == 51u);
    EXPECT_TRUE(y.size() == 51u);
    EXPECT_TRUE(z.size() == 51u);}

// -----------------------------------
// shrinkng (resize with smaller size)
// -----------------------------------

TYPED_TEST(DequeFixture, shr0) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    typename deque_type::value_type a(0);
    typename deque_type::value_type b(1);
    x.push_back(a);
    x.push_back(a);
    x.push_back(b);
    x.push_back(b);
    x.push_back(b);
    x.resize(3);
    EXPECT_TRUE(x.size() == 3u);
    EXPECT_TRUE(x.at(1) == a);}

TYPED_TEST(DequeFixture, shr1) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    for(int i = 0; i < 50; ++i) {
        typename deque_type::value_type a(i);
        x.push_back(a);
    }
    x.resize(25);
    typename deque_type::value_type a(12);
    EXPECT_TRUE(x.size() == 25u);
    EXPECT_TRUE(x.at(12) == a);}

TYPED_TEST(DequeFixture, shr2) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    for(int i = 0; i < 50; ++i) {
        typename deque_type::value_type a(i);
        x.push_front(a);
    }
    x.resize(25);
    typename deque_type::value_type a(37);
    EXPECT_TRUE(x.size() == 25u);
    EXPECT_TRUE(x.at(12) == a);}

// --------------
// element access
// --------------

TYPED_TEST(DequeFixture, ea0) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    typename deque_type::value_type a(0);
    typename deque_type::value_type b(1);
    typename deque_type::value_type c(2);
    x.push_back(a);
    x.push_back(b);
    x.push_back(c);
    EXPECT_TRUE(x.at(0) == x[0]);
    EXPECT_TRUE(x.at(1) == x[1]);
    EXPECT_TRUE(x.at(2) == x[2]);
    EXPECT_TRUE(x.front() == x[0]);
    EXPECT_TRUE(x.back() == x[2]);}

// --------
// capacity
// --------

TYPED_TEST(DequeFixture, cap0) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x;    
    EXPECT_TRUE(x.size() == 0u);
    x.resize(100u);
    EXPECT_TRUE(x.size() == 100u);
    EXPECT_TRUE(!x.empty());
    x.clear();
    EXPECT_TRUE(x.empty());}
