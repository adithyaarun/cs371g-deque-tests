// -------------
// TestDeque.cpp
// -------------

// http://www.cplusplus.com/reference/deque/deque/

// --------
// includes
// --------

#include <deque>     // deque
#include <stdexcept> // invalid_argument, out_of_range

#include "gtest/gtest.h"

#include "Deque.hpp"

// ------
// usings
// ------

using namespace std;
using namespace testing;

// -----
// Types
// -----

template <typename T>
struct DequeFixture : Test {
    using deque_type    = T;
    using allocator_type = typename deque_type::allocator_type;
    using iterator       = typename deque_type::iterator;
    using const_iterator = typename deque_type::const_iterator;
};

template <typename T>
struct IntDequeFixture : Test {
    using deque_type    = T;
    using allocator_type = typename deque_type::allocator_type;
    using iterator       = typename deque_type::iterator;
    using const_iterator = typename deque_type::const_iterator;
};

template <typename T>
struct NestedDequeFixture : Test {
    using deque_type    = T;
    using allocator_type = typename deque_type::allocator_type;
    using iterator       = typename deque_type::iterator;
    using const_iterator = typename deque_type::const_iterator;
};

using all_deque_types =
    Types<
    deque<int>,
    my_deque<int>,
    deque<int, allocator<int>>,
    my_deque<int, allocator<int>>,
    deque<deque<int>>,
    my_deque<deque<int>>,
    deque<deque<int>, allocator<deque<int>>>,
    my_deque<deque<int>, allocator<deque<int>>>>;

using int_deque_types =
    Types<
    deque<int>,
    my_deque<int>,
    deque<int, allocator<int>>,
    my_deque<int, allocator<int>>>;

using nested_deque_types =
    Types<
    deque<deque<int>>,
    my_deque<deque<int>>,
    deque<deque<int>, allocator<deque<int>>>,
    my_deque<deque<int>, allocator<deque<int>>>>;

#ifdef __APPLE__
TYPED_TEST_CASE(DequeFixture, all_deque_types,);
TYPED_TEST_CASE(IntDequeFixture, int_deque_types,);
TYPED_TEST_CASE(NestedDequeFixture, nested_deque_types,);
#else
TYPED_TEST_CASE(DequeFixture, all_deque_types);
TYPED_TEST_CASE(IntDequeFixture, int_deque_types);
TYPED_TEST_CASE(NestedDequeFixture, nested_deque_types);
#endif

// -----
// Tests
// -----

// Original tests
TYPED_TEST(DequeFixture, test0) {
    using deque_type = typename TestFixture::deque_type;
    const deque_type x;
    ASSERT_TRUE(x.empty());
    ASSERT_EQ(x.size(), 0u);
}

TYPED_TEST(DequeFixture, test1) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x;
    iterator b = begin(x);
    iterator e = end(x);
    EXPECT_TRUE(b == e);
}

TYPED_TEST(DequeFixture, test2) {
    using deque_type     = typename TestFixture::deque_type;
    using const_iterator = typename TestFixture::const_iterator;
    const deque_type x;
    const_iterator b = begin(x);
    const_iterator e = end(x);
    EXPECT_TRUE(b == e);
}

// My first set of extensive/stress tests

// Testing different constructors and making sure they all produce the same result, relies on ==
TYPED_TEST(IntDequeFixture, contruct_equals) {
    using deque_type = typename TestFixture::deque_type;

    vector<int> x = {1, 2, 3};

    deque_type d0(x.size());
    for (unsigned int i = 0; i < x.size(); ++i)
        d0.at(i) = x.at(i);

    deque_type d1(x.size(), 2);
    for (unsigned int i = 0; i < x.size(); ++i)
        if (x.at(i) != 2)
            d1.at(i) = x.at(i);

    deque_type d2({1, 2, 3});

    deque_type d3(d0);
    deque_type d4(d1);
    deque_type d5(d2);
    deque_type d6;
    d6 = d0;

    deque_type arr[] = {d0, d1, d2, d3, d4, d5, d6};
    for (int i = 1; i < 7; ++i)
        EXPECT_TRUE(arr[i - 1] == arr[i]);

    for (int i = 0; i < 7; ++i)
        ASSERT_EQ(arr[i].size(), 3);

    deque_type dn0({});
    deque_type dn1({1, 2});
    deque_type dn2({1, 2, 3, 4});
    deque_type dn3({0, 2, 3});
    for (deque_type dt : {
                dn0, dn1, dn2, dn3
            })
        EXPECT_TRUE(d0 != dt);
}

// Testing comparison ops
TYPED_TEST(IntDequeFixture, comparisons) {
    using deque_type = typename TestFixture::deque_type;
    deque_type d0({5, 4, 6});
    deque_type d1({5, 4, 5});

    ASSERT_LT(d1, d0);
    ASSERT_LE(d1, d0);
    ASSERT_GT(d0, d1);
    ASSERT_GE(d0, d1);

    d1 = d0;
    EXPECT_FALSE(d1 < d0);
    ASSERT_LE(d1, d0);
    ASSERT_LE(d0, d1);
    EXPECT_FALSE(d1 > d0);
    ASSERT_GE(d1, d0);
    ASSERT_GE(d0, d1);

    d1 = {5, 4};
    ASSERT_LT(d1, d0);

    d1 = {};
    ASSERT_LT(d1, d0);

    d1 = {5, 4, 6, 0};
    ASSERT_GT(d1, d0);
}

TYPED_TEST(DequeFixture, data_access) {
    using deque_type = typename TestFixture::deque_type;
    using value_type = typename deque_type::value_type;
    deque_type d0;
    vector<value_type> x;

    for (int i = 0; i < 100; ++i) {
        value_type v{};
        d0.push_back(v);
        x.push_back(v);
    }
    for (int i = 0; i < 100; ++i) {
        ASSERT_EQ(d0[i], x[i]);
        ASSERT_EQ(d0.at(i), x.at(i));
    }

    ASSERT_EQ(d0.back(), x.back());
    ASSERT_EQ(d0.front(), x.front());
}

TYPED_TEST(DequeFixture, size_clear_and_erase) {
    using deque_type = typename TestFixture::deque_type;
    using value_type = typename deque_type::value_type;
    deque_type d0;

    for (int i = 0; i < 100; ++i) {
        value_type v{};
        d0.push_back(v);
    }
    ASSERT_EQ(d0.size(), 100);
    d0.clear();
    ASSERT_EQ(d0.size(), 0);
    ASSERT_TRUE(d0.empty());

    for (int i = 0; i < 100; ++i) {
        value_type v{};
        d0.push_back(v);
    }
    ASSERT_EQ(d0.size(), 100);

    int size = 100;
    for (int i = 7; d0.size() > 0; --size) {
        ASSERT_EQ(d0.size(), size);
        i = (i + 7) % d0.size();
        d0.erase(d0.begin() + i);
    }
    ASSERT_EQ(d0.size(), 0);
    ASSERT_TRUE(d0.empty());
}

TYPED_TEST(IntDequeFixture, insert) {
    using deque_type = typename TestFixture::deque_type;
    deque_type d0;
    vector<int> x;

    for (int i = 0, index = 0; d0.size() < 100; ++i) {
        int v = i * 11 + index * 13;
        index = d0.size() == 0 ? 0 : ((index + 7) % d0.size());
        if (i % 2 == 0) {
            d0.insert(d0.begin() + index, v);
            x.insert(x.begin() + index, v);
        } else {
            d0.insert(d0.end() - index, v);
            x.insert(x.end() - index, v);
        }

        ASSERT_EQ(d0.size(), x.size());
        for (unsigned int j = 0; j < d0.size(); ++j)
            ASSERT_EQ(d0.at(j), x.at(j));
    }
}

TYPED_TEST(IntDequeFixture, pop_back) {
    using deque_type = typename TestFixture::deque_type;
    deque_type d0;
    vector<int> x;

    for (int i = 0, index = 0; d0.size() < 100; ++i) {
        int v = i * 11 + index * 13;
        index = d0.size() == 0 ? 0 : ((index + 7) % d0.size());
        if (i % 2 == 0) {
            d0.insert(d0.begin() + index, v);
            x.insert(x.begin() + index, v);
        } else {
            d0.insert(d0.end() - index, v);
            x.insert(x.end() - index, v);
        }
    }

    while (d0.size() > 0) {
        d0.pop_back();
        x.pop_back();

        ASSERT_EQ(d0.size(), x.size());
        for (unsigned int j = 0; j < d0.size(); ++j)
            ASSERT_EQ(d0.at(j), x.at(j));
    }
}

TYPED_TEST(IntDequeFixture, pop_front) {
    using deque_type = typename TestFixture::deque_type;
    deque_type d0;
    vector<int> x;

    for (int i = 0, index = 0; d0.size() < 100; ++i) {
        int v = i * 11 + index * 13;
        index = d0.size() == 0 ? 0 : ((index + 7) % d0.size());
        if (i % 2 == 0) {
            d0.insert(d0.begin() + index, v);
            x.insert(x.begin() + index, v);
        } else {
            d0.insert(d0.end() - index, v);
            x.insert(x.end() - index, v);
        }
    }

    while (d0.size() > 0) {
        d0.pop_front();
        x.erase(x.begin());

        ASSERT_EQ(d0.size(), x.size());
        for (unsigned int j = 0; j < d0.size(); ++j)
            ASSERT_EQ(d0.at(j), x.at(j));
    }
}

// Second set of tests
TYPED_TEST(DequeFixture, equality0) {
    using deque_type = typename TestFixture::deque_type;
    deque_type a;
    deque_type b;
    EXPECT_TRUE(a == b);
    EXPECT_FALSE(a != b);
}

TYPED_TEST(DequeFixture, equality1) {
    using deque_type = typename TestFixture::deque_type;
    const deque_type a;
    const deque_type b;
    EXPECT_TRUE(a == b);
    EXPECT_FALSE(a != b);
}

TYPED_TEST(IntDequeFixture, equality2) {
    using deque_type = typename TestFixture::deque_type;
    deque_type a{12};
    deque_type b{12};
    EXPECT_TRUE(a == b);
    EXPECT_FALSE(a != b);
}

TYPED_TEST(IntDequeFixture, equality3) {
    using deque_type = typename TestFixture::deque_type;
    deque_type a{3};
    deque_type b{3, 4};
    EXPECT_TRUE(a != b);
    EXPECT_FALSE(a == b);
}

TYPED_TEST(IntDequeFixture, access0) {
    using deque_type = typename TestFixture::deque_type;
    deque_type a{6, 3, 7};
    ASSERT_EQ(a.size(), 3);
    ASSERT_EQ(a[0], 6);
    ASSERT_EQ(a.at(0), 6);
    ASSERT_EQ(a[1], 3);
    ASSERT_EQ(a.at(1), 3);
    ASSERT_EQ(a[2], 7);
    ASSERT_EQ(a.at(2), 7);
}

TYPED_TEST(IntDequeFixture, access1) {
    using deque_type = typename TestFixture::deque_type;
    deque_type a{6, 3, 7};
    
    for (int i = 0; i < 3; ++i)
        a[i] = a[i] * 2;
    ASSERT_EQ(a.size(), 3);
    EXPECT_TRUE(a == (deque_type{12, 6, 14}));

    for (int i = 0; i < 3; ++i)
        a.at(i) = a.at(i) * 2;
    ASSERT_EQ(a.size(), 3);
    EXPECT_TRUE(a == (deque_type{24, 12, 28}));
}

TYPED_TEST(IntDequeFixture, access2) {
    using deque_type = typename TestFixture::deque_type;
    deque_type a{6, 3, 7};
    
    for (int k : {-1, 4, 100}) {
        try {
            a.at(k) = 0;
            ASSERT_TRUE(false);
        } catch (const out_of_range& e) {}
    }
}

TYPED_TEST(IntDequeFixture, insert0) {
    using deque_type = typename TestFixture::deque_type;
    deque_type a;
    for (int i = -30; i < 30; ++i)
        a.push_back(i);
    ASSERT_EQ(a.size(), 60);
    for (int i = 0; i < 60; ++i) {
        ASSERT_EQ(a[i], i - 30);
        ASSERT_EQ(a.at(i), i - 30);
    }
}

TYPED_TEST(IntDequeFixture, insert1) {
    using deque_type = typename TestFixture::deque_type;
    deque_type a;
    for (int i = -30; i < 30; ++i)
        a.push_front(i);
    ASSERT_EQ(a.size(), 60);
    for (int i = 0; i < 60; ++i) {
        ASSERT_EQ(a[i], 29 - i);
        ASSERT_EQ(a.at(i), 29 - i);
    }
}

TYPED_TEST(IntDequeFixture, construct0) {
    using deque_type = typename TestFixture::deque_type;
    deque_type a(30);
    ASSERT_EQ(a.size(), 30);
    for (int i = 0; i < 30; ++i)
        ASSERT_EQ(a[i], 0);
}

TYPED_TEST(IntDequeFixture, construct1) {
    using deque_type = typename TestFixture::deque_type;
    deque_type a(30, 243);
    ASSERT_EQ(a.size(), 30);
    for (int i = 0; i < 30; ++i)
        ASSERT_EQ(a[i], 243);
}

TYPED_TEST(IntDequeFixture, construct2) {
    using deque_type = typename TestFixture::deque_type;
    deque_type a{9, 20, 13, 17};
    deque_type b(a);
    a.push_front(-1);
    ASSERT_EQ(b.size(), 4);
    ASSERT_EQ(b[0], 9);
    ASSERT_EQ(b[1], 20);
    ASSERT_EQ(b[2], 13);
    ASSERT_EQ(b[3], 17);
}

TYPED_TEST(IntDequeFixture, erase0) {
    using deque_type = typename TestFixture::deque_type;
    deque_type a{9, 20, 13, 17};
    a.pop_front();
    ASSERT_EQ(a.size(), 3);
    ASSERT_EQ(a[0], 20);
    ASSERT_EQ(a[1], 13);
    ASSERT_EQ(a[2], 17);
}

TYPED_TEST(IntDequeFixture, erase1) {
    using deque_type = typename TestFixture::deque_type;
    deque_type a{9, 20, 13, 17};
    a.pop_back();
    ASSERT_EQ(a.size(), 3);
    ASSERT_EQ(a[0], 9);
    ASSERT_EQ(a[1], 20);
    ASSERT_EQ(a[2], 13);
}

TYPED_TEST(IntDequeFixture, erase2) {
    using deque_type = typename TestFixture::deque_type;
    deque_type a{9, 20, 13, 17};
    a.pop_front();
    a.pop_back();
    ASSERT_EQ(a.size(), 2);
    ASSERT_EQ(a[0], 20);
    ASSERT_EQ(a[1], 13);
}

TYPED_TEST(IntDequeFixture, erase3) {
    using deque_type = typename TestFixture::deque_type;
    deque_type a(100);
    for (int i = 0; i < 100; ++i)
        a[i] = i;
    ASSERT_EQ(a.size(), 100);
    while (a.size())
        a.pop_front();
    ASSERT_EQ(a.size(), 0);
}

TYPED_TEST(IntDequeFixture, erase4) {
    using deque_type = typename TestFixture::deque_type;
    deque_type a(100);
    for (int i = 0; i < 100; ++i)
        a[i] = i;
    ASSERT_EQ(a.size(), 100);
    while (a.size())
        a.pop_back();
    ASSERT_EQ(a.size(), 0);
}

TYPED_TEST(IntDequeFixture, erase5) {
    using deque_type = typename TestFixture::deque_type;
    deque_type a(100);
    for (int i = 0; i < 100; ++i)
        a[i] = i;
    ASSERT_EQ(a.size(), 100);

    for (int k = 43; a.size(); ) {
        k = (k * k + 1) % a.size();
        a.erase(a.begin() + k);
    }
    ASSERT_EQ(a.size(), 0);
}

TYPED_TEST(DequeFixture, iterator0) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type a(100);
    
    iterator it = a.begin();

    EXPECT_TRUE(it == a.begin());
}

TYPED_TEST(DequeFixture, iterator1) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type a(100);
    
    iterator it(a.begin());

    EXPECT_TRUE(it == a.begin());
}

TYPED_TEST(DequeFixture, iterator2) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type a(100);
    
    iterator it;
    it = a.begin();

    EXPECT_TRUE(it == a.begin());
}

TYPED_TEST(IntDequeFixture, iterator3) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type a(100);
    for (int i = 0; i < 100; ++i)
        a[i] = i * 2 + 1;
    
    iterator it = a.begin();
    ASSERT_EQ(*it, 1);
}

TYPED_TEST(IntDequeFixture, iterator4) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type a(100);
    for (int i = 0; i < 100; ++i)
        a[i] = i * 2 + 1;
    
    iterator it = a.begin();
    ++it;
    ASSERT_EQ(*it, 3);
}

TYPED_TEST(IntDequeFixture, iterator5) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type a(100);
    for (int i = 0; i < 100; ++i)
        a[i] = i * 2 + 1;
    
    iterator it = a.begin();
    it++;
    ASSERT_EQ(*it, 3);
}

TYPED_TEST(IntDequeFixture, iterator6) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type a(100);
    for (int i = 0; i < 100; ++i)
        a[i] = i * 2 + 1;
    
    iterator it = a.end();
    --it;
    ASSERT_EQ(*it, 199);
}

TYPED_TEST(IntDequeFixture, iterator7) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type a(100);
    for (int i = 0; i < 100; ++i)
        a[i] = i * 2 + 1;
    
    iterator it = a.end();
    it--;
    ASSERT_EQ(*it, 199);
}

TYPED_TEST(IntDequeFixture, iterator8) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type a(100);
    for (int i = 0; i < 100; ++i)
        a[i] = i * 2 + 1;
    
    iterator it = a.begin();
    ASSERT_EQ(*(it + 5), 11);
}

TYPED_TEST(IntDequeFixture, iterator9) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type a(100);
    for (int i = 0; i < 100; ++i)
        a[i] = i * 2 + 1;
    
    iterator it = a.begin();
    it += 6;
    ASSERT_EQ(*it, 13);
    it += 43;
    ASSERT_EQ(*it, 99);
}

TYPED_TEST(IntDequeFixture, iterator10) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type a(100);
    for (int i = 0; i < 100; ++i)
        a[i] = i * 2 + 1;
    
    iterator it = a.end();
    it -= 3;
    ASSERT_EQ(*it, 195);
    it -= 57;
    ASSERT_EQ(*it, 81);
}

TYPED_TEST(DequeFixture, clear_and_resize0) {
    using deque_type = typename TestFixture::deque_type;
    deque_type a(100);

    a.clear();
    ASSERT_EQ(a.size(), 0);

    a.resize(50);
    ASSERT_EQ(a.size(), 50);

    a.resize(20);
    ASSERT_EQ(a.size(), 20);
    ASSERT_FALSE(a.empty());

    a.resize(0);
    ASSERT_EQ(a.size(), 0);
    ASSERT_TRUE(a.empty());

    a.resize(1000);
    ASSERT_EQ(a.size(), 1000);
}