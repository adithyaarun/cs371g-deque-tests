// -------------
// TestDeque.cpp
// -------------

// http://www.cplusplus.com/reference/deque/deque/

// --------
// includes
// --------

#include <deque>     // deque
#include <stdexcept> // invalid_argument, out_of_range

#include "gtest/gtest.h"

#include "Deque.hpp"

// ------
// usings
// ------

using namespace std;
using namespace testing;

// -----
// Types
// -----

template <typename T>
struct DequeFixture : Test {
    using deque_type    = T;
    using allocator_type = typename deque_type::allocator_type;
    using iterator       = typename deque_type::iterator;
    using const_iterator = typename deque_type::const_iterator;
};

using deque_types =
    Types<
            deque<int>,
            my_deque<int>,
            deque<int, allocator<int>>,
            my_deque<int, allocator<int>>
        >;

template <typename T>
struct NDequeFixture : Test {
    using deque_type    = T;
    using allocator_type = typename deque_type::allocator_type;
    using iterator       = typename deque_type::iterator;
    using const_iterator = typename deque_type::const_iterator;
};
using nested_deque_types =
    Types<
            deque<deque<int>>,
            my_deque<deque<int>>,
            deque<deque<int>, allocator<deque<int>>>,
            my_deque<deque<int>, allocator<deque<int>>>
    >;

#ifdef __APPLE__
    TYPED_TEST_CASE(DequeFixture, deque_types);
#else
    TYPED_TEST_CASE(DequeFixture, deque_types);
#endif

#ifdef __APPLE__
    TYPED_TEST_CASE(NDequeFixture, nested_deque_types);
#else
    TYPED_TEST_CASE(NDequeFixture, nested_deque_types);
#endif
// -----
// Tests
// -----

TYPED_TEST(DequeFixture, test0) {
    using deque_type = typename TestFixture::deque_type;
    const deque_type x;
    ASSERT_TRUE(x.empty());
    ASSERT_EQ(x.size(), 0u);}

TYPED_TEST(DequeFixture, test1) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x;
    iterator b = begin(x);
    iterator e = end(x);
    EXPECT_TRUE(b == e);}

TYPED_TEST(DequeFixture, test2) {
    using deque_type     = typename TestFixture::deque_type;
    using const_iterator = typename TestFixture::const_iterator;
    const deque_type x;
    const_iterator b = begin(x);
    const_iterator e = end(x);
    EXPECT_TRUE(b == e);}


TYPED_TEST(DequeFixture, test3) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type x(4);
    ASSERT_EQ(x.size(), 4u);}


TYPED_TEST(DequeFixture, test4) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type x(67);
    ASSERT_EQ(x.size(), 67u);}

TYPED_TEST(DequeFixture, test5) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type x(2,7);
    ASSERT_EQ(x.size(), 2u);
    ASSERT_TRUE(equal(begin(x), end(x), begin({7, 7})));}


TYPED_TEST(DequeFixture, test6) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type x(0, 3);  // edge case when size is 0
    ASSERT_EQ(x.size(), 0u);
    ASSERT_TRUE(x.empty());}

TYPED_TEST(DequeFixture, test7) {
    using deque_type     = typename TestFixture::deque_type;
    using allocator_type = typename TestFixture::allocator_type;
    deque_type x(2,7, allocator_type());
    ASSERT_EQ(x.size(), 2u);
    ASSERT_TRUE(equal(begin(x), end(x), begin({7, 7})));}


TYPED_TEST(DequeFixture, test8) {
    using deque_type     = typename TestFixture::deque_type;
    using allocator_type = typename TestFixture::allocator_type;
    deque_type x(0, 3, allocator_type());
    ASSERT_EQ(x.size(), 0u);
    ASSERT_TRUE(x.empty());}

TYPED_TEST(DequeFixture, test9) {
    using deque_type     = typename TestFixture::deque_type;
    using allocator_type = typename TestFixture::allocator_type;
    const initializer_list<int> y = {1, 2, 3, 4, 5, 6};
    deque_type x(y, allocator_type());
    ASSERT_EQ(x.size(), 6);
    x.pop_back();
    ASSERT_TRUE(equal(begin(x), end(x), begin({1, 2, 3, 4, 5})));
    ASSERT_TRUE(equal(begin(y), end(y), begin({1, 2, 3, 4, 5, 6})));}

TYPED_TEST(DequeFixture, test10) {
    using deque_type     = typename TestFixture::deque_type;
    const deque_type y(7, 3);s
    ASSERT_EQ(y.size(), 7);
    deque_type x(y);
    ASSERT_EQ(x.size(), 7);
    ASSERT_NE(&x[0], &y[0]);
    ASSERT_TRUE(equal(begin(x), end(x), begin(y)));
}

TYPED_TEST(DequeFixture, test11) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type x(0, 1);
    deque_type y(1, 7);
    x = y;
    ASSERT_TRUE(equal(begin(x), end(x), begin(y)));
}

TYPED_TEST(DequeFixture, test12) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type x(0, 3);
    deque_type y(5, 7);
    x = y;
    ASSERT_TRUE(equal(begin(x), end(x), begin(y)));
}

TYPED_TEST(DequeFixture, test13) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type x(5, 7);
    deque_type y(87, 3);
    x = y;
    ASSERT_TRUE(equal(begin(x), end(x), begin(y)));
}

TYPED_TEST(DequeFixture, test14) {
    using deque_type     = typename TestFixture::deque_type;
    const deque_type x(50, 1);
    ASSERT_EQ(x[25], 1);}

TYPED_TEST(DequeFixture, test15) {
    using deque_type     = typename TestFixture::deque_type;
    const deque_type x(50, 2);
    ASSERT_EQ(x.at(25), 2);}

TYPED_TEST(DequeFixture, test16) {
    using deque_type     = typename TestFixture::deque_type;
    const deque_type x(10, 6);
    ASSERT_EQ(x.back(), 6);}

TYPED_TEST(DequeFixture, test17) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type x(10, 6);
    x.push_back(7);
    ASSERT_EQ(x.back(), 7);
    x.pop_back();
    ASSERT_EQ(x.back(), 6);}

TYPED_TEST(DequeFixture, test18) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type x(10, 6);
    ASSERT_EQ(*begin(x), 6);
    x.push_front(4);
    ASSERT_EQ(*begin(x), 4);}

TYPED_TEST(DequeFixture, test19) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type x(2, 8);
    ASSERT_TRUE(equal(begin(x), end(x), begin({8,8})));
    x.clear();
    ASSERT_TRUE(x.empty());}

TYPED_TEST(DequeFixture, test20) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type x(4, 6);
    ASSERT_TRUE(equal(begin(x), end(x), begin({6,6,6,6})));
    x.clear();
    ASSERT_TRUE(x.empty());}

TYPED_TEST(DequeFixture, test21) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type x(0);
    ASSERT_TRUE(x.empty());}

TYPED_TEST(DequeFixture, test22) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type x(2, 8);
    ASSERT_TRUE(!x.empty());}

TYPED_TEST(DequeFixture, test23) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type x(50, 1);
    ASSERT_EQ(*(--end(x)), 1);  
    x.push_back(4);
    ASSERT_EQ(*(--end(x)), 4);}

TYPED_TEST(DequeFixture, test24) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type x(24, 3);
    ASSERT_EQ(*(--end(x)), 3);  
    x.push_back(7);
    ASSERT_EQ(*(--end(x)), 7);}

TYPED_TEST(DequeFixture, test25) {
    using deque_type     = typename TestFixture::deque_type;
    initializer_list<int> y = {1, 2, 3};
    deque_type x(y);
    x.erase(begin(x));
    ASSERT_EQ(*(begin(x)), 2);
    ASSERT_EQ(x.size(), 2);}

TYPED_TEST(DequeFixture, test26) {
    using deque_type     = typename TestFixture::deque_type;
    const deque_type x(50, 1);
    ASSERT_EQ(x.front(), 1);}

TYPED_TEST(DequeFixture, test27) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type x(50, 1);
    x.push_front(3);
    ASSERT_EQ(x.front(), 3);
    x.pop_front();
    ASSERT_EQ(x.front(), 1);}

TYPED_TEST(DequeFixture, test29) {
    using deque_type     = typename TestFixture::deque_type;
    initializer_list<int> y = {1, 2, 3};
    deque_type x(y);
    x.insert(++begin(x), 78);
    ASSERT_TRUE(equal(begin(x), end(x), begin({1,78,2,3})));}

TYPED_TEST(DequeFixture, test30) {
    using deque_type     = typename TestFixture::deque_type;
    initializer_list<int> y = {1, 2, 3, 4, 5};
    deque_type x(y);
    x.insert(end(x), 9);
    ASSERT_TRUE(equal(begin(x), end(x), begin({1,2,3,4,5,9})));}

TYPED_TEST(DequeFixture, test31) {
    using deque_type     = typename TestFixture::deque_type;
    initializer_list<int> y = {1, 2, 3, 4};
    deque_type x(y);
    x.pop_back();
    ASSERT_TRUE(equal(begin(x), end(x), begin({1,2,3})));}

TYPED_TEST(DequeFixture, test32) {
    using deque_type     = typename TestFixture::deque_type;
    initializer_list<int> y = {1, 2, 3, 4, 5};
    deque_type x(y);
    x.pop_back();
    x.pop_back();
    x.pop_back();
    x.pop_back();
    x.pop_back();
    ASSERT_TRUE(x.empty());
    ASSERT_EQ(x.size(), 0);}

TYPED_TEST(DequeFixture, test33) {
    using deque_type     = typename TestFixture::deque_type;
    initializer_list<int> y = {1, 2, 3, 4};
    deque_type x(y);
    x.pop_front();
    ASSERT_TRUE(equal(begin(x), end(x), begin({2,3,4})));}

TYPED_TEST(DequeFixture, test34) {
    using deque_type     = typename TestFixture::deque_type;
    initializer_list<int> y = {1, 2, 3, 4, 5};
    deque_type x(y);
    x.pop_front();
    x.pop_front();
    x.pop_front();
    x.pop_front();
    x.pop_front();
    ASSERT_TRUE(x.empty());
    ASSERT_EQ(x.size(), 0);}

TYPED_TEST(DequeFixture, test35) {
    using deque_type     = typename TestFixture::deque_type;
    initializer_list<int> y = {1, 2, 3};
    deque_type x(y);
    x.push_back(5);
    ASSERT_TRUE(equal(begin(x), end(x), begin({1,2,3,5})));}

TYPED_TEST(DequeFixture, test36) {
    using deque_type     = typename TestFixture::deque_type;
    initializer_list<int> y = {1, 2, 3};
    deque_type x(y);
    x.push_back(3);
    x.push_back(2);
    x.push_back(69);
    ASSERT_TRUE(!x.empty());
    ASSERT_EQ(x.size(), 6);}

TYPED_TEST(DequeFixture, test37) {
    using deque_type     = typename TestFixture::deque_type;
    initializer_list<int> y = {1, 2, 3};
    deque_type x(y);
    x.push_front(76);
    ASSERT_TRUE(equal(begin(x), end(x), begin({76,1,2,3})));}

TYPED_TEST(DequeFixture, test38) {
    using deque_type     = typename TestFixture::deque_type;
    initializer_list<int> y = {1, 2, 3};
    deque_type x(y);
    x.resize(6, 2);       
    ASSERT_TRUE(equal(begin(x), end(x), begin({1,2,3,2,2,2})));}

TYPED_TEST(DequeFixture, test39) {
    using deque_type     = typename TestFixture::deque_type;
    initializer_list<int> y = {2, 3, 4};
    deque_type x = y;
    x.resize(3);   
    ASSERT_TRUE(equal(begin(x), end(x), begin(y)));}

TYPED_TEST(DequeFixture, test40) {
    using deque_type     = typename TestFixture::deque_type;
    initializer_list<int> y = {2, 3, 4};
    deque_type x(y);
    x.resize(0);     
    ASSERT_TRUE(x.empty());}

TYPED_TEST(DequeFixture, test41) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type x(0,67);
    ASSERT_TRUE(x.size() == 0);}

TYPED_TEST(DequeFixture, test42) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type x(50,1);
    ASSERT_TRUE(x.size() == 50);
    x.resize(7);
    ASSERT_TRUE(x.size() == 7);}
